# LeasePlan assigment

### Clone the project from GilLab:
- git clone git@gitlab.com:leaseplan2871330/lp-assignement.git

### Install Maven and Java on your local machine:
- brew install maven
- brew install java
- setup JAVA_HOME

### Run verify for project:
- mvn verify
- or load maven project in IDEA

### Reports:
- Serenity report
- Report location file: target/site/serenity/index.htl
 
### Write new test cases:
- git checkout -b 'branch_name'
- develop some changes
- git commit -m 'message about changes'
- git pull
- git push
- create PR



## Refactoring:
- Renamed class carsAPI (proper naming productAPI)
- Refactored incorrect steps 
- Refactored feature file structure (added test examples to outline scenario)
- Updated README file
- Refactored steps related to the search functionality 
