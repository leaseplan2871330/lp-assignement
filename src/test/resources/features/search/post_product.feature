Feature: Search functionality for available/unavailable products

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Search products by title
    When the admin calls the "<product>" by endpoint
    Then results should contain records with "<record>" "<item>"
    And we expect the response status "<statusCode>"

    Examples:
      |product|record|item          |statusCode|
      |orange |title |Fanta Orange  |200       |
      |apple  |title |Apple Bandit  |200       |
      |pasta  |title |AH Biologisch |200       |
      |cola   |title |Coca-Cola     |200       |

  Scenario Outline: Search products with a promo status
    When the admin calls the "<product>" by endpoint
    Then results should contain "<promo>" state with the "<progress>"
    And we expect the response status "<statusCode>"

    Examples:
      |product|promo   |progress   |statusCode|
      |orange |isPromo |true   |200       |
      |apple  |isPromo |false  |200       |
      |pasta  |isPromo |true   |200       |
      |cola   |isPromo |false  |200       |


  Scenario Outline: Search action for negative case
    When the admin calls the "<product>" by endpoint
    Then he does not see the results

    Examples:
      |product|
      |car    |