package starter.stepdefinitions;

import net.serenitybdd.rest.SerenityRest;

public class ProductAPI {
    public static String productUrl = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";

    public void getProduct(String name) {
        SerenityRest.given().get(ProductAPI.productUrl + name);
    }
}
