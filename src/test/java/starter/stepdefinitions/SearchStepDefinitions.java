package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {
    ProductAPI productAPI = new ProductAPI();
    @When("the admin calls the {string} by endpoint")
    public void the_admin_calls_the_by_endpoint(String product) {productAPI.getProduct(product);}
    @Then("results should contain records with {string} {string}")
    public void results_should_contain_records_for(String record, String item) {
        restAssuredThat(response ->
                response.body(record, hasItems(containsStringIgnoringCase(item))));
    }
    @Then("results should contain {string} state with the {string}")
    public void results_should_contain_state_for(String state, String progress) {
        restAssuredThat(response ->
                response.body(state, hasItems(equalTo(Boolean.parseBoolean(progress)))));
    }
    //results should contain "<promo>" with the "<progress>"
    @Then("we expect the response status {string}")
    public void we_expect_the_response_status(String statusCode) {
        restAssuredThat(response -> response.statusCode(Integer.parseInt(statusCode)));
    }
    @Then("he does not see the results")
    public void he_does_not_see_the_results() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }
}
